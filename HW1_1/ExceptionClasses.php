<?php

class FailedCanelSubscriptionKey extends \Exception
{

    const FAILED_CANCEL_SUBSCRIPTION_KEY = 1;

    const CODE = 422;


}

class NotFoundActiveSubscriptionKey extends \Exception
{

    const NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY = 2;

    const CODE = 404;

}

