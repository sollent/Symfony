<?php

namespace Academy\Data\Subscription;

class Subscription
{
    /**
     * @var SubscriptionPlan
     */
    private $plan;

    /**
     * @var integer
     */
    private $gatewayId;

    /**
     * Subscription constructor.
     *
     * @param int $id
     */
    public function __construct($id)
    {
        $this->plan = new SubscriptionPlan($id);
        $this->gatewayId = $id;
    }


    /**
     * @return SubscriptionPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}