<?php

require_once "ExceptionClasses.php";

class User
{
    private $id;

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}

class Subscription
{
    /**
     * @var SubscriptionPlan
     */
    private $plan;

    /**
     * @var integer
     */
    private $gatewayId;

    /**
     * Subscription constructor.
     *
     * @param int $id
     */
    public function __construct($id)
    {
        $this->plan = new SubscriptionPlan($id);
        $this->gatewayId = $id;
    }


    /**
     * @return SubscriptionPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}

class SubscriptionPlan
{
    const FREE = 1;
    const PAID = 2;

    private $id;

    /**
     * SubscriptionPlan constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

class SubscriptionRepository
{
    public function getActiveSubscription(User $user)
    {
        // condition in order to emulate error
        if ($user->getId() % 10 != 0) {
            return new Subscription($user->getId());
        } else {
            return null;
        }
    }
}

class SubscriptionGatewayApi
{

    /**
     * @param $id
     * @throws Exception
     */
    public function cancel($id)
    {
        // emulates error
        if ($id % 101 == 0) {
            throw new Exception();
        }
    }
}

class Response
{
    /**
     * @var integer
     */
    private $code;

    private $data;

    /**
     * Response constructor.
     * @param int $code
     * @param $data
     */
    public function __construct($code, $data = [])
    {
        $this->code = $code;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}

class SubscriptionService
{

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionGatewayApi
     */
    private $subscriptionGatewayApi;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->subscriptionGatewayApi = new SubscriptionGatewayApi();
    }


    /**
     * @param $user
     * @return array
     * @throws FailedCanelSubscriptionKey
     * @throws NotFoundActiveSubscriptionKey
     */
    public function cancelSubscription($user)
    {

        $activeSubscription = $this->subscriptionRepository->getActiveSubscription($user);
        if (!empty($activeSubscription)) {
            try {
                $this->subscriptionGatewayApi->cancel($activeSubscription->getGatewayId());
            } catch (\Exception $exception) {
                throw new FailedCanelSubscriptionKey();
            }
        } else {
            throw new NotFoundActiveSubscriptionKey();
        }

    }
}

class SubscriptionController
{
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->subscriptionService = new SubscriptionService();
    }


    public function cancelSubscriptionForUser($userId)
    {

        try{

            $user = new User($userId);
            $this->subscriptionService->cancelSubscription($user);

            return new Response(200);

        } catch (FailedCanelSubscriptionKey $e) {
            return new Response(FailedCanelSubscriptionKey::CODE, [
               'message' => 'The subscription wasn\'t cancelled due to technical issue'
            ]);
        } catch (NotFoundActiveSubscriptionKey $e) {
            return new Response(NotFoundActiveSubscriptionKey::CODE, [
                'message' => 'Subscription is not found for user'
            ]);
        }


    }
}

