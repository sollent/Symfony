<?php

namespace Academy\Data\Subscription;

class SubscriptionPlan
{
    const FREE = 1;
    const PAID = 2;

    private $id;

    /**
     * SubscriptionPlan constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}