<?php

namespace Academy\ExceptionClasses;

class FailedCanelSubscriptionKey extends \Exception
{

    const FAILED_CANCEL_SUBSCRIPTION_KEY = 1;

    const CODE = 422;


}